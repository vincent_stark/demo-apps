require 'spec_helper'

describe "Relationships" do

  before(:each) do
    @user = Factory(:user)
    @other_user = Factory(:user, :email => Factory.next(:email))
    visit signin_path
    fill_in :email,    :with => @user.email
    fill_in :password, :with => @user.password
    click_button
  end

  describe "follow" do

    it "should increase following and change button caption" do
      lambda do
        visit user_path(@other_user)
        response.should have_selector("input#relationship_submit", :value => "Follow")
        click_button
        response.should have_selector("input#relationship_submit", :value => "Unfollow")
        response.should have_selector("a", :href => followers_user_path(@other_user),
                                           :content => "1 follower")
        visit root_path
        response.should have_selector("a", :href => following_user_path(@user),
                                           :content => "1 following")
        
      end.should change(Relationship, :count).by(1)
    end
  end
  
  describe "unfollow" do

    it "should decrease following and change button caption" do
      visit user_path(@other_user)
      click_button
      lambda do      
        click_button
        response.should have_selector("input#relationship_submit", :value => "Follow")
        response.should have_selector("a", :href => followers_user_path(@other_user),
                                           :content => "0 follower")
        visit root_path
        response.should have_selector("a", :href => following_user_path(@user),
                                           :content => "0 following")
        
      end.should change(Relationship, :count).by(-1)
    end
  end
end
