document.observe("dom:loaded", function() {
  changeCounter();
});

function changeCounter() {

  var counterClass;

  var charsLeft =
    140 - document.getElementById("micropost_content").value.length;

  if (charsLeft <= 140 && charsLeft > 50) {
    counterClass = "counter_norm";
  }
  else if (charsLeft <= 50 && charsLeft > 0) {
    counterClass = "counter_warn";
  }
  else {
    counterClass = "counter_err";
  }
  
  document.getElementById("counter").innerHTML = charsLeft;
  document.getElementById("counter").setAttribute("class", counterClass);

}
